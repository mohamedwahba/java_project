/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server.db.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import static tectactoeapp.server.db.models.BaseModel.conn;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author maged
 */
public class Game {
    
    public int id;
    public int player_x_id;
    public int player_y_id;
    public LocalTime game_time;
    
    public boolean saved = false;
    public static String tablename = "game";
    
    public static Vector<User> cache = new Vector<>();
    
    public Game(){}
    
    public Game(int player_x, int player_y, LocalTime game_time){
        this.player_x_id = player_x;
        this.player_y_id = player_y;
        this.game_time = game_time;
    }

    private Game(int id, int player_x, int player_y, LocalTime game_time){
        this(player_x, player_y, game_time);
        this.id = id;
    }
    
    public static Game create(int player_x, int player_y, LocalTime game_time){
        Game g = new Game(player_x, player_y, game_time);
        if(g.save()){
            return g;
        }
        return null;
    }
    
    public static Game create(int player_x, int player_y){
        return create(player_x, player_y, LocalTime.now());
    }
    
    public boolean save(){
        if(saved) return true;
        String query = null;
        query = "INSERT INTO " + tablename + " (player_x, player_y, game_time) VALUES (?, ?, ?)";
        try{
            String[] keys = {"id"};
            PreparedStatement stm = conn.prepareStatement(query, keys);
            stm.setInt(1, player_x_id);
            stm.setInt(2, player_y_id);
            stm.setTime(3, Time.valueOf(game_time));
            int affected = stm.executeUpdate();
            if(affected == 0) throw new SQLException("Couldn't save the user object.");
            try(ResultSet gKeys = stm.getGeneratedKeys()){
                if(gKeys.next()){
                    id = gKeys.getInt(1);
                    saved = true;
                    return true;
                }
                else 
                    throw new SQLException("Couldn't save the user object.");
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
            return false;
        }
        
    }
    
    public List<User> getPlayers(){
        List<User> players = new ArrayList<>();
        players.add(User.find(player_x_id));
        players.add(User.find(player_y_id));
        return players;
    }
    
    public List<Move> getMoves(){
        List<Move> moves = new ArrayList<>();
        try{
            PreparedStatement stm = conn.prepareStatement("SELECT id FROM " + tablename + "AS g JOIN " + Move.tablename + " AS m ON g.id = m.game_id WHERE g.id = " + id);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                moves.add(Move.find(rs.getInt("id")));
            }
            return moves;
        } catch(SQLException e){
            return null;
        }
    }
    
    
    
    
    
    //Move Class.
    public static class Move{
        public static String tablename = "moves";
        
        public int id;
        public int game_id;
        public int owner_id;
        public int user_id;
        public int move;
        public LocalTime move_time;
        
        public boolean saved = false;
        
        public Move(){}
        
        public Move(int game_id, int owner_id, int user_id, int move, LocalTime move_time){
            this.game_id = game_id;
            this.owner_id = owner_id;
            this.user_id = user_id;
            this.move_time = move_time;
        }
        public Move(int id, int game_id, int owner_id, int user_id, int move, LocalTime move_time){
            this(game_id, owner_id, user_id, move, move_time);
            this.id = id;
        }
        
        public boolean save(){
        if(saved) return true;
        String query = null;
        query = "INSERT INTO " + tablename + " (game_id, owner_id, user_id, move, move_time) VALUES (?, ?, ?, ?, ?)";
        try{
            String[] keys = {"id"};
            PreparedStatement stm = conn.prepareStatement(query, keys);
            stm.setInt(1, game_id);
            stm.setInt(2, owner_id);
            stm.setInt(3, user_id);
            stm.setInt(4, move);
            stm.setTime(5, Time.valueOf(move_time));
            int affected = stm.executeUpdate();
            if(affected == 0) throw new SQLException("Couldn't save the user object.");
            try(ResultSet gKeys = stm.getGeneratedKeys()){
                if(gKeys.next()){
                    id = gKeys.getInt(1);
                    saved = true;
                    return true;
                }
                else 
                    throw new SQLException("Couldn't save the user object.");
            }
            
            } catch(SQLException e){
                //e.printStackTrace();
                return false;
            }
        }
        
        public static Move find(int id){
            try{
                Statement stm = conn.createStatement();
                ResultSet rs = stm.executeQuery("SELECT * FROM " + tablename + " WHERE id = " + id);
                if(rs.next()){
                    Move m = new Move(rs.getInt("id"), rs.getInt("owner_id"), rs.getInt("game_id"), rs.getInt("user_id"), rs.getTime("move_time").toLocalTime());
                    m.saved = true;
                    return m;
                }
                else return null;     
            } catch(SQLException e){
                return null;
            }
        }
        public static Move create(int game_id, int owner_id, int user_id, int move, LocalTime move_time){
            Move m = new Move(game_id, owner_id, user_id, move, move_time);
            if(m.save()){
                return m;
            }
            return null;
        }
    
        public static Move create(int game_id, int owner_id, int user_id, int move){
            return create(game_id, owner_id, user_id, move, LocalTime.now());
        }
        
    }
    
    
    //Winners class
    public static class Winner{
        
        public int id;
        public int game_id;
        public int user_id;
    }
}
