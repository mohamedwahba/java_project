/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server.db.models;

/**
 *
 * @author maged
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Vector;
import java.util.Map;

public class User extends BaseModel{
    public int id;
    public String username;
    public String password;
    public String email;
    public long score;
    public Socket connection = null;
    public PrintStream writer = null;
    public BufferedReader reader = null;
    
    public boolean logged = false;

    public static String tablename = "users";
    
    public static Vector<User> cache = new Vector<>();
    
    public User(){}
    
    public User(String username, String password, String email, long score){
        this.username = username;
        this.password = password;
        this.email = email;
        this.score = score;
    }

    private User(int id, String username, String password, String email, long score){
        this(username, password, email, score);
        this.id = id;
    }
    
    public static User create(String username, String password, String email, long score){
        User user = new User(username, password, email, score);
        if(user.save()){
            return user;
        }
        return null;
    }
    
    public boolean save(){
        if(saved) return true;
        String query = null;
        query = "INSERT INTO " + tablename + " (username, email, password) VALUES (?, ?, ?)";
        try{
            String[] keys = {"id"};
            PreparedStatement stm = conn.prepareStatement(query, keys);
            stm.setString(1, username);
            stm.setString(2, email);
            stm.setString(3, password);
            int affected = stm.executeUpdate();
            if(affected == 0) throw new SQLException("Couldn't save the user object.");
            try(ResultSet gKeys = stm.getGeneratedKeys()){
                if(gKeys.next()){
                    id = gKeys.getInt(1);
                    saved = true;
                    return true;
                }
                else 
                    throw new SQLException("Couldn't save the user object.");
            }
            
        } catch(SQLException e){
            //e.printStackTrace();
            return false;
        }
        
    }
    public static User find(int id){
        try{
            Statement stm = conn.createStatement();
            try(ResultSet rs = stm.executeQuery("SELECT * FROM " + tablename + " WHERE id = " + id)){
                if(rs.next()){
                    User user = new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getLong("score"));
                    user.saved = true;
                    return user;
                }
                else return null;
            }       
        } catch(SQLException e){
            return null;
        }
      
    }
    
    public static User findBy(String field, String value){
        Map<String, String> m = new HashMap<>();
        m.put(field, value);
        return User.findBy(m);
    }
    
    public static User findBy(Map<String, String> entries){
        StringBuilder filter = new StringBuilder();
        boolean empty = true;
        if(!entries.isEmpty())
            filter.append(" WHERE ");
        for(Map.Entry<String, String> e : entries.entrySet()){
            if(!empty)
                filter.append(" AND ");
            filter.append(e.getKey() + " = " + "\'" + e.getValue() + "\'");
            empty = false;
        }
        try{
            Statement stm = conn.createStatement();
            try(ResultSet rs = stm.executeQuery("SELECT * FROM " + tablename + filter)){
                if(rs.next()){
                    User user = new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getLong("score"));
                    user.saved = true;
                    return user;
                }
                else return null;
            }       
        } catch(SQLException e){
            return null;
        }
    }
        
    public boolean isAuthenticated(){
        return isAuthenticated(this.email, this.password);
    }
    
    public static boolean isAuthenticated(User user){
        return user.isAuthenticated();
    }
    
    public static boolean isAuthenticated(String email, String password){
        User user = User.findBy(email, password);
        if(user == null)
            return false;
        return true;
    }
    
    public static User getLoggedUser(String username){
        for(User player : cache){
            if(player.username.equals(username)){
                return player;
            }
        }
        return null;
    }

    public Socket getConnection(){
        return connection;
    }
    public BufferedReader getReader(){
        return reader;
    }
    public PrintStream getWriter(){
        return writer;
    }
    public void setConnection(Socket connection){
        this.connection = connection;
    }
    public void setReader(BufferedReader reader){
        this.reader = reader;
    }
    public void setWriter(PrintStream writer){
        this.writer = writer;
    }
    public void setRemote(Socket conn, BufferedReader reader, PrintStream writer){
        setConnection(connection);
        setReader(reader);
        setWriter(writer);
    }
            
    
}
