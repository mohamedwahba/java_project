/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server.db;

/**
 *
 * @author maged
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DataBaseConnection {
    //TODO read them from conf file.
    private String host;
    private String user;
    private String pass;
    private static DataBaseConnection dbConn = null;
    private Connection conn = null;
    
    private DataBaseConnection(){}
    
    public static DataBaseConnection newInstance(){
        if(dbConn == null)
            dbConn = new DataBaseConnection();
        return dbConn;
    }
    
    public Connection connect(){
        return connect("jdbc:mysql://localhost:3306/tectactoe", "root", "Maged0502344773");
    }
    
    
    public Connection connect(String host, String user, String pass){
        this.host = host;
        this.user = user;
        this.pass = pass;
        
        try{
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            conn = DriverManager.getConnection(this.host, this.user, this.pass);
            
        } catch(SQLException e){
            e.printStackTrace();
        } finally{
            return conn;
        }
    }

}
