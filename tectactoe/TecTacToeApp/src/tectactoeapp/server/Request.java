/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maged
 */
public class Request {
    public Map<String, String> headers = new HashMap<>();
    
    public Request(String request){
        String[] lines = request.split(", ");
        for(String line : lines){
            String[] header = line.split(" : ");
            headers.put(header[0], header[1]);
        }
    }
    
    public String getType(){
        return headers.get("Type");
    }
    
    public String get(String header){
        return headers.get(header);
    }
    
    public static void main(String[] args){
        String req = "Type : Login, Username : maged, Password : 123456\n";
        Request r = new Request(req);
        System.out.println(r.get("Username"));
    }
}
