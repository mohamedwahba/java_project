/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server;

/**
 *
 * @author maged
 */

import java.io.BufferedReader;
import java.net.Socket;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import tectactoeapp.server.db.models.User;

public class RequestHandler extends Thread{
    
    private Socket client;
    private BufferedReader reader;
    private PrintStream response;
    
    public RequestHandler(Socket clientSoc){
        try{
            client = clientSoc;
            System.out.println("New Connection: " + client);
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            response = new PrintStream(client.getOutputStream());
            start();
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public void run(){
        while(true){
            try{
                String reqString = null;
                reqString = reader.readLine();
                if(reqString == null) throw new IOException();
                Request req = new Request(reqString);
                switch(req.getType()){
                    case "Login":
                        handleLogin(req);
                        break;
                    case "Register":
                        handleRegister(req);
                        break;
                    case "Invitation":
                        handleInvitation(req);
                        break;
                    case "Invitation-Reply":
                        handleInvitationReply(req);
                        break;
                    case "Player-Move":
                        handlePlayerMove(req);
                        break;
                    case "Invitation-Canceled":
                        break;
                }

            } catch(IOException e){
                System.out.println("error");
                response.println("Type : Connection-Lost");
                try{
                    reader.close();
                    response.close();
                    client.close();
                e.printStackTrace();
                } catch(IOException er){
                    er.printStackTrace();
                }
            }
        }     
    }
    
    public void handleLogin(Request req){
        String username = req.get("Username");
        String password = req.get("Password");
        Map<String, String> by = new HashMap<>();
        by.put("username", username);
        by.put("password", password);
        User user = User.findBy(by);
        if(user == null)
            response.println("Type : Login-Faild");
        else{
            user.setRemote(client, reader, response);
            User.cache.add(user);  //login
            response.println("Type : Login-Succeeded, Score : " + user.score + ", Username : " + user.username );
            //System.out.println("User " + user.username + " Logged in.");
        }
    }
    
    public void handleRegister(Request req){
        String username = req.get("Username");
        String password = req.get("Password");
        String email = req.get("Email");
        User user = User.create(username, password, email, 0);
        if(user == null)
            response.println("Type : Register-Faild");
        else{
            user.setRemote(client, reader, response);
            User.cache.add(user);
            response.println("Type : Register-Succeeded, Score : " + 0 + ", Username : " + username );
        }
    }

    private void handleInvitation(Request req) {
        // Type : Invitation, From : playername, To : otherplayername, Record : True/False
        User player = User.findBy("username", req.get("From"));
        User otherPlayer = null;
        for(User u : User.cache){
            if(u.username.equals(req.get("To")) && !player.username.equals(req.get("To"))){
                otherPlayer = u;
                break;
            }
        }
        try{
            if(otherPlayer == null){
                response.println("Type : Invitation-Faild, To : " + req.get("To"));
                return;
            }
            else{
                System.out.println(player);
                boolean recordX = (req.get("Record").equals("True") ? true : false);
                GameManager gm = new GameManager(player, otherPlayer, recordX, recordX);
                response.println("Type : Invitation-Sent, Game-ID : " + gm.game.id);
                PrintStream otherPlayerWriter = new PrintStream(otherPlayer.getConnection().getOutputStream());
                System.out.println(otherPlayerWriter);
                otherPlayerWriter.println("Type : Invitation-Request, Game-ID : " + gm.game.id + ", From : " + player.username);
                
            }
            
        } catch(IOException e){
            response.println("Type : Connection-Lost");     
        }
    }

    private void handlePlayerMove(Request req) {
        String move = req.get("Move");
        int gameId = Integer.parseInt(req.get("Game-ID"));
        GameManager gm = GameManager.getGameManager(gameId);
        if(gm != null){
            gm.play(move);
        }
        else{
            response.println("Type : Move-Faild");
            return;
        }
    }

    private void handleInvitationReply(Request req) {
        //When player Y replies to player X request
        String reply = req.get("Reply");
        PrintStream otherPlayerWriter = null;
        try{
            int gameId = Integer.parseInt(req.get("Game-ID"));
            GameManager gm = GameManager.getGameManager(gameId);
            if(gm == null){
                response.println("Type : Connction-Lost");
                return;
            }
            otherPlayerWriter = new PrintStream(gm.playerX.getConnection().getOutputStream());
            if(reply.equals("Accepted")){
                gm.recordedY = req.get("Record").equals("True") ? true : false;
                gm.start();
                otherPlayerWriter.println("Type : Invitation-Response, Status : True");
            }
            else if(reply.equals("Denied")){
                otherPlayerWriter.println("Type : Invitation-Response, Status : False");
            }
        } catch(IOException e){
            otherPlayerWriter.println("Type : Invitation-Response, Status : True");
        }
    }
    
    
}
