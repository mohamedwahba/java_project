/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server;

/**
 *
 * @author maged
 */

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.sql.*;

import tectactoeapp.server.db.DataBaseConnection;

public class Server {
    private ServerSocket serverSoc;
    private DataBaseConnection dbConn;
    
    public Server(int port){
        System.out.println("Connecting...");
        try{
            serverSoc = new ServerSocket(port);
            dbConn = DataBaseConnection.newInstance();
            System.out.println("Listening on port " + port);
            while(true){
                Socket client = serverSoc.accept();
                new RequestHandler(client);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
    
}
