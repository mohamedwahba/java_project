/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tectactoeapp.server;

/**
 *
 * @author maged
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Vector;
import tectactoeapp.server.db.models.Game;
import tectactoeapp.server.db.models.User;


public class GameManager{
    
    public boolean xTurn = true;
    public User playerX;
    public User playerY;
    public Game game;
    public boolean recordedX;
    public boolean recordedY;
    public boolean gameStarted = false;
    
    public static Vector<GameManager> games;
    
    public BufferedReader playerXReader;
    public BufferedReader playerYReader;
    public PrintStream playerXWriter;
    public PrintStream playerYWriter;
                    
    public GameManager(User player, User otherPlayer, boolean recordX, boolean recordedY){
        this.playerX = player;
        this.playerY = otherPlayer;
        this.recordedX = recordX;
        this.recordedY = recordedY;
            playerXReader = playerX.getReader();
            playerYReader = playerY.getReader();
            playerXWriter = playerX.getWriter();
            playerYWriter = playerY.getWriter();
            game = Game.create(playerX.id, playerY.id);
            if(game == null){
                playerYWriter.println("Type : Connection-Lost");
                playerXWriter.println("Type : Connection-Lost");
                return;
            }
        
            games.add(this);
        }
        
        //start();
    
    
    /*public void run(){
        
        try{
            playerXReader = new BufferedReader(new InputStreamReader(playerX.getConnection().getInputStream()));
            playerYReader = new BufferedReader(new InputStreamReader(playerY.getConnection().getInputStream()));
            playerXWriter = new PrintWriter(playerX.getConnection().getOutputStream());
            playerYWriter = new PrintWriter(playerY.getConnection().getOutputStream());
            while(!gameEnd){
                if(xTurn){
                    String move = playerXReader.readLine();
                    if(move == null) throw new IOException("X");
                    Request req = new Request(move);
                    playerYWriter.println(move);
                    if(recordedX)
                        Game.Move.create(game.id, playerX.id, playerX.id, Integer.parseInt(req.get("Move")));
                    if(recordedY)
                        Game.Move.create(game.id, playerY.id, playerX.id, Integer.parseInt(req.get("Move")));
                    if(req.get("Win-Status").equals("True")){
                        
                    }
                    xTurn = false;
                }
                else{
                    String move = playerYReader.readLine();
                    if(move == null) throw new IOException("Y");
                    Request req = new Request(move);
                    playerXWriter.println(move);
                    if(recordedX)
                        Game.Move.create(game.id, playerX.id, playerY.id, Integer.parseInt(req.get("Move")));
                    if(recordedY)
                        Game.Move.create(game.id, playerY.id, playerY.id, Integer.parseInt(req.get("Move")));
                    xTurn = true;
                }
            }
        } catch(IOException e){
            String msg = e.getMessage();
            if(msg.equals("X")){
                playerYWriter.println("Type : Connection-Lost");
            }
            else if(msg.equals("Y")){
                playerXWriter.println("Type : Connection-Lost");
            }
            else{
                playerYWriter.println("Type : Connection-Lost");
                playerXWriter.println("Type : Connection-Lost");
            }
            return;
        }*/
        
    public void start(){
        gameStarted = true;
    }
    public void play(String move){
            if(!gameStarted) return;
            try{
                
          
                if(xTurn){
                    //String move = playerXReader.readLine();
                    if(move == null) throw new IOException("X");
                    Request req = new Request(move);
                    playerYWriter.println(move);
                    if(recordedX)
                        Game.Move.create(game.id, playerX.id, playerX.id, Integer.parseInt(req.get("Move")));
                    if(recordedY)
                        Game.Move.create(game.id, playerY.id, playerX.id, Integer.parseInt(req.get("Move")));
                    if(req.get("Win-Status").equals("True")){
                        
                    }
                    xTurn = false;
                }
                else{
                    //String move = playerYReader.readLine();
                    if(move == null) throw new IOException("Y");
                    Request req = new Request(move);
                    playerXWriter.println(move);
                    if(recordedX)
                        Game.Move.create(game.id, playerX.id, playerY.id, Integer.parseInt(req.get("Move")));
                    if(recordedY)
                        Game.Move.create(game.id, playerY.id, playerY.id, Integer.parseInt(req.get("Move")));
                    xTurn = true;
                }
            
            } catch(IOException e){
                String msg = e.getMessage();
                if(msg.equals("X")){
                    playerYWriter.println("Type : Connection-Lost");
                }
                else if(msg.equals("Y")){
                    playerXWriter.println("Type : Connection-Lost");
                }
                else{
                    playerYWriter.println("Type : Connection-Lost");
                    playerXWriter.println("Type : Connection-Lost");
                }
                return;
        
        }
    }
        
    public static GameManager getGameManager(int id){
        for(GameManager gm : games){
            if(gm.game.id == id){
                return gm;
            }
        }
        return null;
    }
        
    
}
