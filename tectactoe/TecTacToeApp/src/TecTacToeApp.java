/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author maged
 */

import tectactoeapp.server.Server;

public class TecTacToeApp {
    
    public static void main(String[] args){
        int port = 5555;
        if(args.length > 0)
            port = Integer.parseInt(args[0]);
        new Server(port);
    }
}
