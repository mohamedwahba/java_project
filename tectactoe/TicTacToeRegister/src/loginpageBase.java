
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class loginpageBase extends Pane {

    protected final ImageView imageView;
    protected final TextField userName;
    protected final TextField password;
    protected final Button login;
    protected final ImageView imageView0;
    protected final Button back;
    protected Stage pstage;

    public loginpageBase(Stage primaryStage) {
        pstage = primaryStage;

        imageView = new ImageView();
        userName = new TextField();
        password = new TextField();
        login = new Button();
        login.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("login");

            }
        });

        imageView0 = new ImageView();
        back = new Button();
        back.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainWindowBase root = new mainWindowBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setResizable(false);
                primaryStage.setTitle("Game");
                primaryStage.setScene(scene);
                primaryStage.show();

            }
        });

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(453.0);
        setPrefWidth(617.0);

        imageView.setFitHeight(453.0);
        imageView.setFitWidth(617.0);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image(getClass().getResource("cover-start.gif").toExternalForm()));

        userName.setLayoutX(389.0);
        userName.setLayoutY(81.0);
        userName.setPrefHeight(34.0);
        userName.setPrefWidth(202.0);
        userName.setPromptText("userName");
        userName.setStyle("-fx-font-size: 18;");

        password.setLayoutX(391.0);
        password.setLayoutY(184.0);
        password.setPrefHeight(34.0);
        password.setPrefWidth(202.0);
        password.setPromptText("password");
        password.setStyle("-fx-font-size: 18;");

        login.setLayoutX(445.0);
        login.setLayoutY(294.0);
        login.setMnemonicParsing(false);
        login.setPrefHeight(42.0);
        login.setPrefWidth(91.0);
        login.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        login.setText("login");
        login.setId("btn");

        login.setTextFill(javafx.scene.paint.Color.WHITE);
        login.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        imageView0.setFitHeight(468.0);
        imageView0.setFitWidth(365.0);
        imageView0.setLayoutX(10.0);
        imageView0.setLayoutY(-5.0);
        imageView0.setPickOnBounds(true);
        imageView0.setImage(new Image(getClass().getResource("cover-start.png").toExternalForm()));

        back.setLayoutX(39.0);
        back.setLayoutY(378.0);
        back.setMnemonicParsing(false);
        back.setPrefHeight(42.0);
        back.setPrefWidth(91.0);
        back.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        back.setText("Back");
        back.setId("btn");

        back.setTextFill(javafx.scene.paint.Color.WHITE);
        back.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        getChildren().add(imageView);
        getChildren().add(userName);
        getChildren().add(password);
        getChildren().add(login);
        getChildren().add(imageView0);
        getChildren().add(back);
        
        login.addEventHandler(ActionEvent.ACTION, (e) -> {
            try{
                TicTacToe.server = new Socket(InetAddress.getLocalHost(), 5555);
                TicTacToe.reader = new DataInputStream(TicTacToe.server.getInputStream());
                TicTacToe.writer = new PrintStream(TicTacToe.server.getOutputStream());
                TicTacToe.writer.println("Type : Login, Username : " + userName.getText() + ", Password : " + password.getText());
                String res = TicTacToe.reader.readLine();
                if(res == null) throw new IOException();
                TicTacToe.Response response = new TicTacToe.Response(res);
                if(response.getType().equals("Login-Succeeded")){
                    TicTacToe.logged = true;
                    TicTacToe.username = response.get("Username");
                    TicTacToe.score = Integer.parseInt(response.get("Score"));
                    TicTacToe.changeScene(primaryStage, new finallogin((primaryStage)));
                }
            }catch(IOException err){
                 try{
                    TicTacToe.writer.close();
                    TicTacToe.reader.close();
                    TicTacToe.server.close();
                } catch(IOException er){
                    er.printStackTrace();
                }
            }
        });

    }

}
