/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author mohamed
 */
public class TicTacToe extends Application {

    public Stage primaryStage;
    public static Socket server;
    public static PrintStream writer;
    public static DataInputStream reader;
    public static boolean logged = false;
    public static String username;
    public static int score;
    public static int gameID = -1;
    public static boolean busy = false;
    public static volatile Boolean socketBusy = false;
    @Override
    public void start(Stage primaryStage) {
//    RegisterBase root = new RegisterBase(primaryStage);
//        Scene scene = new Scene(root, 617, 453);
//        scene.getStylesheets().add(getClass().getResource("styles.css").toString());
//        primaryStage.setTitle("Registeration");
//        primaryStage.setScene(scene);
//        primaryStage.show();

//        loginBase root = new loginBase(primaryStage);
//        Scene scene = new Scene(root, 617, 453);
//        primaryStage.setResizable(false);
//        primaryStage.setTitle("Game");
//        primaryStage.setScene(scene);
//        primaryStage.show();
//
       this.primaryStage = primaryStage;
       mainWindowBase root = new mainWindowBase(primaryStage);
        Scene scene = new Scene(root, 617, 453);
       scene.getStylesheets().add(getClass().getResource("styles.css").toString());

        primaryStage.setResizable(false);
        primaryStage.setTitle("Game");
        primaryStage.setScene(scene);
        primaryStage.show();



//
//
//     GameBaseComputer root = new GameBaseComputer();
//        Scene scene = new Scene(root, 617, 453);
//        primaryStage.setTitle("Game with computer");
//        primaryStage.setScene(scene);
//        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
    
    
    public static class Response {
        public Map<String, String> headers = new HashMap<>();

        public Response(String request){
            String[] lines = request.split(", ");
            for(String line : lines){
                String[] header = line.split(" : ");
                headers.put(header[0], header[1]);
            }
        }

        public String getType(){
            return headers.get("Type");
        }

        public String get(String header){
            return headers.get(header);
        }
    

    }
    
    public static void changeScene(Stage stage, Pane root){
        Scene scene = new Scene(root, 617, 453);
        //scene.getStylesheets().add(getClass().getResource("styles.css").toString());
        stage.setTitle("Registeration");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
    
    
    
    //Reciever Thread that accepts requests and moves.
    class Reciever extends Thread{
        public Stage stage;
        
        public Reciever(Stage stage){
            this.stage = stage;
        }
        public void run(){
            while(true){
                synchronized(socketBusy){
                    if(socketBusy) continue;
                }
                
                try{
                    String res = reader.readLine();
                    if(res == null){
                        throw new IOException();
                    }
                    Response resObj = new Response(res);
                    
                    switch(resObj.getType()){
                        case "Invitation-Request":
                            handleInvitationRequest(resObj);
                            break;
                        case "Player-Move":
                            //handleLoginResponse(resObj);
                            break;
                    }
                } catch(IOException e){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Sorry! Server is down, try again later.");
                        alert.showAndWait(); 
                        mainWindowBase root = new mainWindowBase(stage);
                        Scene scene = new Scene(root, 617, 453);
                        scene.getStylesheets().add(getClass().getResource("styles.css").toString());
                        stage.setTitle("Registeration");
                        stage.setScene(scene);
                        stage.setResizable(false);
                        stage.show();
                    
                }
            }
        }
        
        void handleInvitationRequest(Response res){
            if(TicTacToe.busy){
                TicTacToe.writer.println("Type : Invitation-Reply, Reply : Denied");
                return;
            }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Invitation Sent From " + res.get("From") + ".", ButtonType.YES, ButtonType.NO);
            alert.showAndWait();
            if(alert.getResult().equals(ButtonType.YES)){
                TicTacToe.writer.println("Type : Invitation-Reply, Reply : Accepted");
                TicTacToe.changeScene(primaryStage, new GameOnline(primaryStage));
            }
            else if(alert.getResult().equals(ButtonType.NO)){
                TicTacToe.writer.println("Type : Invitation-Reply, Reply : Denied");
            }
            
        }
    }

}
