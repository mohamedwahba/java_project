
import java.io.IOException;
import java.net.SocketTimeoutException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class onlineChoiceBase extends Pane {

    protected final ImageView imageView;
    protected final Button playRandom;
    protected final Button inviteFriend;
    protected final CheckBox checkBox;
    protected final Button back;
    protected Stage pstage;

    public onlineChoiceBase(Stage primaryStage) {
        pstage = primaryStage;

        imageView = new ImageView();
        playRandom = new Button();
        playRandom.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("playRandom");

            }
        });
        inviteFriend = new Button();
        inviteFriend.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                synchronized(TicTacToe.socketBusy){
                    TicTacToe.socketBusy = true;
                }
                String otherPlayer = JOptionPane.showInputDialog(this, "Enter username: ");
                String checked = checkBox.isSelected() ? "True" : "False";
                try{
                    TicTacToe.server.setSoTimeout(60000); // wait for 2 minute
                    TicTacToe.writer.println("Type : Invitation, To : " + otherPlayer + ", From : " + TicTacToe.username + ", Record : " + checked);
                    System.out.println("Type : Invitation, To : " + otherPlayer + ", From : " + TicTacToe.username + ", Record : " + checked);
                    String res = TicTacToe.reader.readLine();
                    if(res == null) throw new IOException();
                    TicTacToe.Response response = new TicTacToe.Response(res);
                    
                    if(response.getType().equals("Invitation-Faild")){
                        Alert a = new Alert(Alert.AlertType.INFORMATION, "Invitation Faild, pleas try again.", ButtonType.OK);
                        a.showAndWait();
                    }
                    else if(response.getType().equals("Connection-Lost")){
                        Alert a = new Alert(Alert.AlertType.INFORMATION, "Connection Lost, pleas try again.", ButtonType.OK);
                        a.showAndWait();
                    }
                    else if(response.getType().equals("Invitation-Sent")){
                        TicTacToe.gameID = Integer.parseInt(response.get("Game-ID"));
                        Alert a = new Alert(Alert.AlertType.INFORMATION, "Invitation Sent, Please Wait for other player.", ButtonType.CANCEL);
                        a.showAndWait();
                        if(a.getResult().equals(ButtonType.CANCEL)){
                            TicTacToe.writer.println("Type : Invitation-Canceled, To : " + otherPlayer + ", From : " + TicTacToe.username);
                            TicTacToe.gameID = -1;
                        }
                    }
                    
                } catch(SocketTimeoutException err){
                    Alert a = new Alert(Alert.AlertType.INFORMATION, "Invitation Faild, pleas try again.", ButtonType.OK);
                    a.showAndWait();
                } catch(IOException e){
                    Alert a = new Alert(Alert.AlertType.INFORMATION, "Connection Lost, pleas try again.", ButtonType.OK);
                    a.showAndWait();
                } 
                synchronized(TicTacToe.socketBusy){
                    TicTacToe.socketBusy = false;
                }
            }
        });
        checkBox = new CheckBox();
        checkBox.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("checkBox");

            }
        });
        back = new Button();
        back.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("back");
                finallogin root = new finallogin(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("start");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();

            }
        });

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(453.0);
        setPrefWidth(617.0);

        imageView.setFitHeight(453.0);
        imageView.setFitWidth(617.0);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image(getClass().getResource("tic-tac-toe-screenshot.jpg").toExternalForm()));

        playRandom.setLayoutX(136.0);
        playRandom.setLayoutY(166.0);
        playRandom.setMnemonicParsing(false);
        playRandom.setPrefHeight(26.0);
        playRandom.setPrefWidth(343.0);
        playRandom.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        playRandom.setText("Play Random");
        playRandom.setId("btn");

        playRandom.setTextFill(javafx.scene.paint.Color.WHITE);
        playRandom.setFont(new Font("Noto Sans CJK KR Bold", 31.0));

        inviteFriend.setLayoutX(136.0);
        inviteFriend.setLayoutY(293.0);
        inviteFriend.setMnemonicParsing(false);
        inviteFriend.setPrefHeight(26.0);
        inviteFriend.setPrefWidth(343.0);
        inviteFriend.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        inviteFriend.setText("Invite Friend");
        inviteFriend.setId("btn");

        inviteFriend.setTextFill(javafx.scene.paint.Color.WHITE);
        inviteFriend.setFont(new Font("Noto Sans CJK KR Bold", 31.0));

        checkBox.setLayoutX(435.0);
        checkBox.setLayoutY(383.0);
        checkBox.setMnemonicParsing(false);
        checkBox.setPrefHeight(43.0);
        checkBox.setPrefWidth(157.0);
        checkBox.setText("Record");
        checkBox.setTextFill(javafx.scene.paint.Color.WHITE);
        checkBox.setFont(new Font(20.0));

        back.setLayoutX(22.0);
        back.setLayoutY(392.0);
        back.setMnemonicParsing(false);
        back.setPrefHeight(43.0);
        back.setPrefWidth(78.0);
        back.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        back.setText("back");
        back.setId("btn");

        back.setTextFill(javafx.scene.paint.Color.WHITE);
        back.setFont(new Font("Noto Sans CJK KR Bold", 15.0));

        getChildren().add(imageView);
        getChildren().add(playRandom);
        getChildren().add(inviteFriend);
        getChildren().add(checkBox);
        getChildren().add(back);

    }
}
