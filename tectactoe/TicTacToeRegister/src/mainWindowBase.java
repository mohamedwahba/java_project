
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class mainWindowBase extends Pane {

    protected final ImageView imageView;
    protected final Button register;
    protected final Button login;
    protected final Button start;
    protected Stage pstage;

    public mainWindowBase(Stage primaryStage) {
        pstage = primaryStage;

        imageView = new ImageView();
        register = new Button();
        register.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RegisterBase root = new RegisterBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                        scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("Register");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();

            }
        });
        login = new Button();
        login.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
       
                loginpageBase root = new loginpageBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                        scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("login");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();

            }
        });
        start = new Button();
        start.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                finallogin root = new finallogin(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                        scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("start");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();

            }
        });

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(453.0);
        setPrefWidth(617.0);

        imageView.setFitHeight(453.0);
        imageView.setFitWidth(617.0);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image(getClass().getResource("cover-start.png").toExternalForm()));

        register.setLayoutX(86.0);
        register.setLayoutY(266.0);
        register.setMnemonicParsing(false);
        register.setPrefHeight(42.0);
        register.setPrefWidth(108.0);
        register.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        register.setText("Register");
                register.setId("btn");

        register.setTextFill(javafx.scene.paint.Color.WHITE);
        register.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        login.setLayoutX(435.0);
        login.setLayoutY(260.0);
        login.setMnemonicParsing(false);
        login.setPrefHeight(42.0);
        login.setPrefWidth(91.0);
        login.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        login.setText("login");
                        login.setId("btn");

        login.setTextFill(javafx.scene.paint.Color.WHITE);
        login.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        start.setLayoutX(263.0);
        start.setLayoutY(308.0);
        start.setMnemonicParsing(false);
        start.setPrefHeight(42.0);
        start.setPrefWidth(91.0);
        start.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        start.setTextFill(javafx.scene.paint.Color.WHITE);
                                start.setId("btn");

        start.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        getChildren().add(imageView);
        getChildren().add(start);
        
        
        if(!TicTacToe.logged){
            getChildren().add(login);
            getChildren().add(register);
    
        }
    }
}
