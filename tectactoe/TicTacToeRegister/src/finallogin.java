
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class finallogin extends Pane {

    protected final ImageView imageView;
    protected final Button twoPlayer;
    protected final Button withComputer;
    protected final Button online;
    protected final Button back;
    protected final Button exit;
    protected Stage pstage;

    public finallogin(Stage primaryStage) {
        pstage = primaryStage;

        imageView = new ImageView();
        twoPlayer = new Button();
        twoPlayer.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("twoPlayer");
                GameBase root = new GameBase();
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("two player");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();
            }
        });
        withComputer = new Button();
        withComputer.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("withComputer");
                GameBaseComputer root = new GameBaseComputer();
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setTitle("with comuter");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();
            }
        });
        online = new Button();
        online.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(!TicTacToe.logged){
                    Alert a = new Alert(Alert.AlertType.CONFIRMATION, "You need to login, Do you want to login?", ButtonType.YES, ButtonType.NO);
                    a.showAndWait();
                    if(a.getResult().equals(ButtonType.YES)){
                        TicTacToe.changeScene(pstage, new loginpageBase(pstage));
                    }
                    return;
                }
                onlineChoiceBase root = new onlineChoiceBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setResizable(false);
                primaryStage.setTitle("Game");
                primaryStage.setScene(scene);
                primaryStage.show();
            }
        });
        back = new Button();
        back.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("back");
                mainWindowBase root = new mainWindowBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());

                primaryStage.setResizable(false);
                primaryStage.setTitle("Game");
                primaryStage.setScene(scene);
                primaryStage.show();
            }
        });
        exit = new Button();
        exit.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("exit");
                System.out.println("exit");
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "do you want to exit" + " ?", ButtonType.YES, ButtonType.NO);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    System.exit(0);

                } else if (alert.getResult() == ButtonType.NO) {

                }
            }
        });

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(453.0);
        setPrefWidth(617.0);

        imageView.setFitHeight(453.0);
        imageView.setFitWidth(617.0);
        imageView.setLayoutX(-1.0);
        imageView.setOpacity(0.94);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image(getClass().getResource("tic-tac-toe-screenshot.jpg").toExternalForm()));

        twoPlayer.setLayoutX(126.0);
        twoPlayer.setLayoutY(93.0);
        twoPlayer.setMnemonicParsing(false);
        twoPlayer.setPrefHeight(26.0);
        twoPlayer.setPrefWidth(343.0);
        twoPlayer.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        twoPlayer.setText("Two Player ");
        twoPlayer.setId("btn");
        twoPlayer.setTextFill(javafx.scene.paint.Color.WHITE);
        twoPlayer.setFont(new Font("Noto Sans CJK KR Bold", 31.0));

        withComputer.setLayoutX(125.0);
        withComputer.setLayoutY(189.0);
        withComputer.setMnemonicParsing(false);
        withComputer.setPrefHeight(26.0);
        withComputer.setPrefWidth(343.0);
        withComputer.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        withComputer.setText("Play with computer");
        withComputer.setId("btn");

        withComputer.setTextFill(javafx.scene.paint.Color.WHITE);
        withComputer.setFont(new Font("Noto Sans CJK KR Bold", 28.0));

        online.setLayoutX(122.0);
        online.setLayoutY(293.0);
        online.setMnemonicParsing(false);
        online.setPrefHeight(26.0);
        online.setPrefWidth(343.0);
        online.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        online.setText("Play Online");
        online.setId("btn");

        online.setTextFill(javafx.scene.paint.Color.WHITE);
        online.setFont(new Font("Noto Sans CJK KR Bold", 31.0));

        back.setLayoutX(14.0);
        back.setLayoutY(397.0);
        back.setMnemonicParsing(false);
        back.setPrefHeight(42.0);
        back.setPrefWidth(91.0);
        back.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        back.setText("back");
        back.setId("btn");

        back.setTextFill(javafx.scene.paint.Color.WHITE);
        back.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        exit.setLayoutX(512.0);
        exit.setLayoutY(397.0);
        exit.setMnemonicParsing(false);
        exit.setPrefHeight(42.0);
        exit.setPrefWidth(91.0);
        exit.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 2px;");
        exit.setText("Exit");
        exit.setId("btn");

        exit.setTextFill(javafx.scene.paint.Color.WHITE);
        exit.setFont(new Font("Noto Sans CJK KR Bold", 16.0));

        getChildren().add(imageView);
        getChildren().add(twoPlayer);
        getChildren().add(withComputer);
        getChildren().add(online);
        getChildren().add(back);
        getChildren().add(exit);

    }
}
