
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class GameOnline extends GridPane {

    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final ColumnConstraints columnConstraints1;
    protected final ColumnConstraints columnConstraints2;
    protected final ColumnConstraints columnConstraints3;
    protected final RowConstraints rowConstraints;
    protected final RowConstraints rowConstraints0;
    protected final RowConstraints rowConstraints1;
    protected final Button buttonNo1;
    protected final Button buttonNo3;
    protected final Button buttonNo4;
    protected final Button buttonNo6;
    protected final Button buttonNo7;
    protected final Button buttonNo8;
    protected final Button buttonNo9;
    protected final Button buttonNo2;
    protected final Button buttonNo5;
    //protected final Button reset;
    protected final Button exit;
    protected final Label label;
    protected final Label label0;
    protected final Label label1;
    protected final Label label2;
    private String startGame = "X";
    private int xcount = 0;
    private int ocount = 0;

    protected Stage primaryStage;
    
    public GameOnline(Stage primaryStage) {
        this.primaryStage = primaryStage;
        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        columnConstraints1 = new ColumnConstraints();
        columnConstraints2 = new ColumnConstraints();
        columnConstraints3 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        rowConstraints0 = new RowConstraints();
        rowConstraints1 = new RowConstraints();
        buttonNo1 = new Button();

        buttonNo1.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("1");
                handleMove(buttonNo1);

            }
        });

        buttonNo3 = new Button();
        buttonNo3.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("3");
                handleMove(buttonNo3);
            }
        });
        buttonNo4 = new Button();
        buttonNo4.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("4");
                handleMove(buttonNo4);
            }
        });
        buttonNo6 = new Button();
        buttonNo6.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("6");
                handleMove(buttonNo6);

            }
        });
        buttonNo7 = new Button();
        buttonNo7.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("7");

                handleMove(buttonNo7);

            }
        });
        buttonNo8 = new Button();
        buttonNo8.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("8");
                handleMove(buttonNo8);
            }
        });

        buttonNo9 = new Button();

        buttonNo9.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("9");
                handleMove(buttonNo9);

            }
        });
        
        buttonNo2 = new Button();
        buttonNo2.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("2");
                handleMove(buttonNo2);

            }
        });
        buttonNo5 = new Button();

        buttonNo5.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("5");
                handleMove(buttonNo5);
            }
        });
        
        exit = new Button();

        exit.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.CONFIRMATION, "do you want to exit" + " ?", ButtonType.YES, ButtonType.NO);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    TicTacToe.changeScene(primaryStage, new finallogin(primaryStage));

                }

            }
        });
        label = new Label();
        label0 = new Label();
        label1 = new Label();
        label2 = new Label();

        setGridLinesVisible(true);
        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(400.0);
        setPrefWidth(600.0);

        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        columnConstraints1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(100.0);

        columnConstraints2.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints2.setMinWidth(10.0);
        columnConstraints2.setPrefWidth(100.0);

        columnConstraints3.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints3.setMinWidth(10.0);
        columnConstraints3.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints0.setMinHeight(10.0);
        rowConstraints0.setPrefHeight(30.0);
        rowConstraints0.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints1.setMinHeight(10.0);
        rowConstraints1.setPrefHeight(30.0);
        rowConstraints1.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        buttonNo1.setMnemonicParsing(false);
        buttonNo1.setPrefHeight(152.0);
        buttonNo1.setPrefWidth(215.0);

        GridPane.setColumnIndex(buttonNo3, 2);
        buttonNo3.setMnemonicParsing(false);
        buttonNo3.setPrefHeight(162.0);
        buttonNo3.setPrefWidth(238.0);

        GridPane.setRowIndex(buttonNo4, 1);
        buttonNo4.setMnemonicParsing(false);
        buttonNo4.setPrefHeight(198.0);
        buttonNo4.setPrefWidth(291.0);

        GridPane.setColumnIndex(buttonNo6, 2);
        GridPane.setRowIndex(buttonNo6, 1);
        buttonNo6.setMnemonicParsing(false);
        buttonNo6.setPrefHeight(178.0);
        buttonNo6.setPrefWidth(206.0);

        GridPane.setRowIndex(buttonNo7, 2);
        buttonNo7.setMnemonicParsing(false);
        buttonNo7.setPrefHeight(220.0);
        buttonNo7.setPrefWidth(276.0);

        GridPane.setColumnIndex(buttonNo8, 1);
        GridPane.setRowIndex(buttonNo8, 2);
        buttonNo8.setMnemonicParsing(false);
        buttonNo8.setPrefHeight(156.0);
        buttonNo8.setPrefWidth(241.0);

        GridPane.setColumnIndex(buttonNo9, 2);
        GridPane.setRowIndex(buttonNo9, 2);
        buttonNo9.setMnemonicParsing(false);
        buttonNo9.setPrefHeight(191.0);
        buttonNo9.setPrefWidth(212.0);

        GridPane.setColumnIndex(buttonNo2, 1);
        buttonNo2.setMnemonicParsing(false);
        buttonNo2.setPrefHeight(258.0);
        buttonNo2.setPrefWidth(395.0);

        GridPane.setColumnIndex(buttonNo5, 1);
        GridPane.setRowIndex(buttonNo5, 1);
        buttonNo5.setMnemonicParsing(false);
        buttonNo5.setPrefHeight(169.0);
        buttonNo5.setPrefWidth(254.0);

        //GridPane.setColumnIndex(reset, 3);
        //GridPane.setRowIndex(reset, 2);
        //reset.setMnemonicParsing(false);
        //reset.setPrefHeight(177.0);
        //reset.setPrefWidth(126.0);
        //reset.setText("Reset");
        //reset.setTextFill(javafx.scene.paint.Color.valueOf("#512222"));
        //reset.setFont(new Font(26.0));

        GridPane.setColumnIndex(exit, 4);
        GridPane.setRowIndex(exit, 2);
        exit.setMnemonicParsing(false);
        exit.setPrefHeight(204.0);
        exit.setPrefWidth(126.0);
        exit.setText("Exit");
        exit.setFont(new Font(36.0));

        GridPane.setColumnIndex(label, 3);
        label.setPrefHeight(177.0);
        label.setPrefWidth(136.0);
        label.setText(TicTacToe.username);
        label.setFont(new Font(25.0));

        GridPane.setColumnIndex(label0, 3);
        GridPane.setRowIndex(label0, 1);
        label0.setPrefHeight(210.0);
        label0.setPrefWidth(143.0);
        label0.setText("player o");
        label0.setFont(new Font(26.0));

        GridPane.setColumnIndex(label1, 4);
        label1.setPrefHeight(164.0);
        label1.setPrefWidth(117.0);

        GridPane.setColumnIndex(label2, 4);
        GridPane.setRowIndex(label2, 1);
        label2.setPrefHeight(163.0);
        label2.setPrefWidth(158.0);

        getColumnConstraints().add(columnConstraints);
        getColumnConstraints().add(columnConstraints0);
        getColumnConstraints().add(columnConstraints1);
        getColumnConstraints().add(columnConstraints2);
        getColumnConstraints().add(columnConstraints3);
        getRowConstraints().add(rowConstraints);
        getRowConstraints().add(rowConstraints0);
        getRowConstraints().add(rowConstraints1);
        getChildren().add(buttonNo1);
        getChildren().add(buttonNo3);
        getChildren().add(buttonNo4);
        getChildren().add(buttonNo6);
        getChildren().add(buttonNo7);
        getChildren().add(buttonNo8);
        getChildren().add(buttonNo9);
        getChildren().add(buttonNo2);
        getChildren().add(buttonNo5);
        //getChildren().add(reset);
        getChildren().add(exit);
        getChildren().add(label);
        getChildren().add(label0);
        getChildren().add(label1);
        getChildren().add(label2);

    }

    public void handleMove(Button btn){
        btn.setText(startGame);
        if (startGame.equalsIgnoreCase("X")) {
            btn.setStyle("-fx-background-color:green;-fx-font-size: 8em; ");

        } else {
            btn.setStyle("-fx-background-color:red;-fx-font-size: 8em; ");
        }
        chosePlayer();
        btn.setDisable(true);
        winner();
    }
    private void chosePlayer() {
        if (startGame.equalsIgnoreCase("X")) {
            startGame = "O";
        } else {
            startGame = "X";

        }
    }

    private void winner() {
        String b1 = buttonNo1.getText();
        String b2 = buttonNo2.getText();
        String b3 = buttonNo3.getText();
        String b4 = buttonNo4.getText();
        String b5 = buttonNo5.getText();
        String b6 = buttonNo6.getText();
        String b7 = buttonNo7.getText();
        String b8 = buttonNo8.getText();
        String b9 = buttonNo9.getText();

        if ((b1 == "X" && b2 == "X" && b3 == "X")) {

            xcount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo2.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo3.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
              winalert.showAndWait();
            afterWin();

        }
        if (b4 == "X" && b5 == "X" && b6 == "X") {
            xcount++;
            gameScore();
            buttonNo4.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo6.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b7 == "X" && b8 == "X" && b9 == "X") {
            xcount++;
            gameScore();
            buttonNo7.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b7 == "X" && b8 == "X" && b9 == "X") {
            xcount++;
            gameScore();
            buttonNo7.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b1 == "X" && b4 == "X" && b7 == "X") {
            xcount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo4.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo7.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b2 == "X" && b5 == "X" && b8 == "X") {
            xcount++;
            gameScore();
            buttonNo2.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b3 == "X" && b6 == "X" && b9 == "X") {
            xcount++;
            gameScore();
            buttonNo3.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo6.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

           // JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b1 == "X" && b5 == "X" && b9 == "X") {
            xcount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

            //JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }
        if (b3 == "X" && b5 == "X" && b7 == "X") {
            xcount++;
            gameScore();
            buttonNo3.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
            buttonNo7.setStyle("-fx-background-color:yellow;-fx-font-size: 8em;");
              Alert winalert =new Alert(AlertType.INFORMATION,"Player x win");
                            winalert.showAndWait();

          //  JOptionPane.showMessageDialog(null, "player x win");

            afterWin();
        }

        /**
         * *********************************************************
         */
        if ((b1 == "O" && b2 == "O" && b3 == "O")) {
            ocount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo2.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo3.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b4 == "O" && b5 == "O" && b6 == "O") {
            ocount++;
            gameScore();
            buttonNo4.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo6.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b7 == "O" && b8 == "O" && b9 == "O") {
            ocount++;
            gameScore();
            buttonNo7.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
           // JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b7 == "O" && b8 == "O" && b9 == "O") {
            ocount++;
            gameScore();
            buttonNo7.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
           // JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b1 == "O" && b4 == "O" && b7 == "O") {
            ocount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo4.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo7.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b2 == "O" && b5 == "O" && b8 == "O") {
            ocount++;
            gameScore();
            buttonNo2.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo8.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
           // JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b3 == "O" && b6 == "O" && b9 == "O") {
            ocount++;
            gameScore();
            buttonNo3.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo6.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b1 == "O" && b5 == "O" && b9 == "O") {
            ocount++;
            gameScore();
            buttonNo1.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo9.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if (b3 == "O" && b5 == "O" && b7 == "O") {
            ocount++;
            gameScore();

            buttonNo3.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo5.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
            buttonNo7.setStyle("-fx-background-color:cyan;-fx-font-size: 8em;");
                      Alert winalert =new Alert(AlertType.INFORMATION,"Player o win");
                            winalert.showAndWait();
            
            //JOptionPane.showMessageDialog(null, "player O win");

            afterWin();
        }
        if ((buttonNo1.getText() != "") && (buttonNo2.getText() != "") && (buttonNo3.getText() != "") && (buttonNo4.getText() != "") && (buttonNo5.getText() != "") && (buttonNo6.getText() != "") && (buttonNo7.getText() != "") && (buttonNo8.getText() != "") && (buttonNo9.getText() != "")) {
            check();
        }
    }

    private void check() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Game ended" + "\n" + "no winner " + "\n" + "do you want to play again" + " ?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            buttonNo1.setDisable(false);
            buttonNo2.setDisable(false);
            buttonNo3.setDisable(false);
            buttonNo4.setDisable(false);
            buttonNo5.setDisable(false);
            buttonNo6.setDisable(false);
            buttonNo7.setDisable(false);
            buttonNo8.setDisable(false);
            buttonNo9.setDisable(false);
            buttonNo1.setText("");
            buttonNo2.setText("");
            buttonNo3.setText("");
            buttonNo4.setText("");
            buttonNo5.setText("");
            buttonNo6.setText("");
            buttonNo7.setText("");
            buttonNo8.setText("");
            buttonNo9.setText("");
            buttonNo1.setStyle("");
            buttonNo2.setStyle("");
            buttonNo3.setStyle("");
            buttonNo4.setStyle("");
            buttonNo5.setStyle("");
            buttonNo6.setStyle("");
            buttonNo7.setStyle("");
            buttonNo8.setStyle("");
            buttonNo9.setStyle("");

        } else if (alert.getResult() == ButtonType.NO) {
            System.exit(0);
        }
    }

    public void gameScore() {
        label1.setText(String.valueOf(xcount));
        label1.setStyle("-fx-font-size: 8em;");
        label2.setText(String.valueOf(ocount));
        label2.setStyle("-fx-font-size: 8em;");
    }

    private void afterWin() {
//                    JOptionPane.showMessageDialog(null, "player x win");
      
        Alert alert = new Alert(AlertType.CONFIRMATION, "do you want to play again" + " ?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            buttonNo1.setDisable(false);
            buttonNo2.setDisable(false);
            buttonNo3.setDisable(false);
            buttonNo4.setDisable(false);
            buttonNo5.setDisable(false);
            buttonNo6.setDisable(false);
            buttonNo7.setDisable(false);
            buttonNo8.setDisable(false);
            buttonNo9.setDisable(false);
            buttonNo1.setText("");
            buttonNo2.setText("");
            buttonNo3.setText("");
            buttonNo4.setText("");
            buttonNo5.setText("");
            buttonNo6.setText("");
            buttonNo7.setText("");
            buttonNo8.setText("");
            buttonNo9.setText("");
            buttonNo1.setStyle("");
            buttonNo2.setStyle("");
            buttonNo3.setStyle("");
            buttonNo4.setStyle("");
            buttonNo5.setStyle("");
            buttonNo6.setStyle("");
            buttonNo7.setStyle("");
            buttonNo8.setStyle("");
            buttonNo9.setStyle("");

        } else if (alert.getResult() == ButtonType.NO) {
            System.exit(0);
        }

    }
}
