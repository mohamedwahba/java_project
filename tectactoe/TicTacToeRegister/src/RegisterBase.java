
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class RegisterBase extends Pane {

    protected final ImageView imageView;
    protected final ImageView imageView0;
    protected final TextField userName;
    protected final TextField firstName;
    protected final TextField lastName;
    protected final TextField email;
    protected final PasswordField password;
    protected final Button button;
    protected final Button button0;

    protected Stage pstage;
    Statement stmt;
    Connection con;
    PreparedStatement pst;
    List<String> list = new ArrayList<String>();

    public void openconn() throws SQLException {
        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/javaProject", "root", "");
        stmt = con.createStatement();

    }

    public RegisterBase(Stage primaryStage) {
        pstage = primaryStage;

        imageView = new ImageView();
        imageView0 = new ImageView();
        userName = new TextField();
        firstName = new TextField();
        lastName = new TextField();
        email = new TextField();
        password = new PasswordField();
        button = new Button();
        button0 = new Button();

        setMaxHeight(453.0);
        setMaxWidth(617.0);
        setMinHeight(453.0);
        setMinWidth(617.0);
        setPrefHeight(453.0);
        setPrefWidth(617.0);

        imageView.setFitHeight(453.0);
        imageView.setFitWidth(365.0);
        imageView.setPickOnBounds(true);
        imageView.setImage(new Image(getClass().getResource("cover-start.png").toExternalForm()));

        imageView0.setFitHeight(463.0);
        imageView0.setFitWidth(285.0);
        imageView0.setLayoutX(347.0);
        imageView0.setLayoutY(-9.0);
        imageView0.setPickOnBounds(true);
        imageView0.setImage(new Image(getClass().getResource("cover-start.gif").toExternalForm()));

        userName.setLayoutX(379.0);
        userName.setLayoutY(52.0);
        userName.setPrefHeight(34.0);
        userName.setPrefWidth(202.0);
        userName.setPromptText("userName");
        userName.setStyle("-fx-font-size: 18;");

        firstName.setLayoutX(378.0);
        firstName.setLayoutY(128.0);
        firstName.setPrefHeight(34.0);
        firstName.setPrefWidth(202.0);
        firstName.setPromptText("firstName");
        firstName.setStyle("-fx-font-size: 18;");

        lastName.setLayoutX(379.0);
        lastName.setLayoutY(188.0);
        lastName.setPrefHeight(34.0);
        lastName.setPrefWidth(202.0);
        lastName.setPromptText("lastName");
        lastName.setStyle("-fx-font-size: 18;");

        email.setLayoutX(380.0);
        email.setLayoutY(247.0);
        email.setPrefHeight(34.0);
        email.setPrefWidth(202.0);
        email.setPromptText("email");
        email.setStyle("-fx-font-size: 18;");

        password.setLayoutX(381.0);
        password.setLayoutY(306.0);
        password.setPrefHeight(34.0);
        password.setPrefWidth(202.0);
        password.setPromptText("password");
        password.setStyle("-fx-font-size: 18;");

        button.setLayoutX(391.0);
        button.setLayoutY(367.0);
        button.setLineSpacing(1.0);
        button.setMnemonicParsing(false);
        button.setPrefHeight(56.0);
        button.setPrefWidth(168.0);
        button.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 3px; -fx-font-size: 28px; -fx-text-fill: white;");
        button.setText("Register");
        button.setId("btn");
        button.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    if(TicTacToe.server == null){
                        TicTacToe.server = new Socket(InetAddress.getLocalHost(), 5555);
                        TicTacToe.reader = new DataInputStream(TicTacToe.server.getInputStream());
                        TicTacToe.writer = new PrintStream(TicTacToe.server.getOutputStream());
                    }
                    TicTacToe.writer.println("Type : Register, Email : " + email.getText() + ", " + "Username : " + userName.getText() + ", Password : " + password.getText());
                    String res = TicTacToe.reader.readLine();
                    if(res == null) throw new IOException();
                    TicTacToe.Response response = new TicTacToe.Response(res);
                    if(response.getType().equals("Register-Succeeded")){
                        TicTacToe.logged = true;
                        TicTacToe.username = response.get("Username");
                        TicTacToe.score = Integer.parseInt(response.get("Score"));
                        TicTacToe.changeScene(primaryStage, new finallogin((primaryStage)));
                    }
                    else if(response.getType().equals("Register-Faild")){
                        Alert a = new Alert(AlertType.INFORMATION, "Registeration faild, pleas try again.", ButtonType.OK);
                        a.showAndWait();
                        System.out.println("Error");
                    }
                }catch(IOException err){
                     try{
                        TicTacToe.writer.close();
                        TicTacToe.reader.close();
                        TicTacToe.server.close();
                    } catch(IOException er){
                        er.printStackTrace();
                    }
            }

            }

        });

        button0.setLayoutX(14.0);
        button0.setLayoutY(381.0);
        button0.setLineSpacing(1.0);
        button0.setMnemonicParsing(false);
        button0.setPrefHeight(42.0);
        button0.setPrefWidth(104.0);
        button0.setStyle("-fx-background-color: none; -fx-border-color: white; -fx-border-radius: 20px; -fx-border-width: 3px; -fx-font-size: 20px; -fx-text-fill: white;");
        button0.setText("Back");
        button0.setId("btn");

        button0.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("back");
                mainWindowBase root = new mainWindowBase(primaryStage);
                Scene scene = new Scene(root, 617, 453);
                scene.getStylesheets().add(getClass().getResource("styles.css").toString());
                primaryStage.setTitle("Registeration");
                primaryStage.setScene(scene);
                primaryStage.setResizable(false);
                primaryStage.show();
            }

        });

        getChildren().add(imageView);
        getChildren().add(imageView0);
        getChildren().add(userName);
        getChildren().add(firstName);
        getChildren().add(lastName);
        getChildren().add(email);
        getChildren().add(password);
        getChildren().add(button);
        getChildren().add(button0);

    }

    public boolean register() {
        try {
            //open connection
            openconn();

            String username = userName.getText();
            String queryString = new String("SELECT `user name` FROM `registeration` WHERE `user name`='" + username + "'");
            ResultSet rs = stmt.executeQuery(queryString);
            if (rs.next() == false) {
                pst = con.prepareStatement("INSERT INTO registeration(`user name`, `first name`, `lastname`, `email`, `password`) VALUES (?,?,?,?,?)");

                Pattern pattern = Pattern.compile("[A-Za-z0-9_]+");

                if ((username != null) && pattern.matcher(username).matches()) {
                    pst.setString(1, username);

                } else {
                    //JOptionPane.showMessageDialog(null, "enter valid username");
                    list.add("enter valid username");
                }

                String firstname = firstName.getText();
                if ((firstname != null) && pattern.matcher(firstname).matches()) {
                    pst.setString(2, firstname);
                } else {
                    //JOptionPane.showMessageDialog(null, "enter valid firstName");
                    list.add("enter valid firstName");

                }

                String lastname = lastName.getText();
                if ((lastname != null) && pattern.matcher(lastname).matches()) {
                    pst.setString(3, lastname);

                } else {
                    // JOptionPane.showMessageDialog(null, "enter valid lastName");
                    list.add("enter valid lastName");
                }
                String Email = email.getText();

                Pattern patternEmail = Pattern.compile("^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$");
                if ((Email != null) && patternEmail.matcher(Email).matches()) {
                    pst.setString(4, Email);

                } else {
                    //JOptionPane.showMessageDialog(null, "enter valid Email");
                    list.add("enter valid Email");
                }

                String pass = password.getText();
                if ((pass != null) && pattern.matcher(pass).matches()) {
                    pst.setString(5, pass);

                } else {
                    //JOptionPane.showMessageDialog(null, "enter valid Password");
                    list.add("enter valid Password");
                }

                String get = "number of error = " + list.size() + "\n";
                for (int i = 0; i < list.size(); i++) {

                    get += list.get(i) + "\n";

                    //System.out.println(list.get(i));
                }
                if (list.size() == 0) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Registeration Done");
                    alert.showAndWait();

                } else {
                    JOptionPane.showMessageDialog(null, get);
                    int OK_CAN = JOptionPane.OK_CANCEL_OPTION;
                    if (OK_CAN == 2) {
                        list.removeAll(list);
                    }
                }

                pst.execute();

//                JOptionPane.showMessageDialog(null, "data inserted");
                userName.setText("");
                firstName.setText("");
                lastName.setText("");
                email.setText("");
                password.setText("");
                return true;

            } else {
                JOptionPane.showMessageDialog(null, "user already Exist ");

            }

        } catch (Exception e) {
            e.getStackTrace();
        }
        return false;
    }
}
